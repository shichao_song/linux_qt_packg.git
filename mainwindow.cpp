#include "mainwindow.h"
#include "ui_mainwindow.h"
#include <QProcess>
#include <QFileDialog>
#include <QDebug>
#include <QSettings>

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);

    //写入配置文件
    QSettings config("./local.ini",QSettings::IniFormat);
    config.beginGroup("PARA");
    exe_path = config.value("EXE_PATH").toString();
    exe_dir = config.value("EXE_DIR").toString();
    passwd = config.value("PASSWD").toString();
    config.endGroup();
    config.sync();

    ui->lineEdit->setText(exe_path);
    ui->lineEdit_3->setText(passwd);
}

MainWindow::~MainWindow()
{
    delete ui;
}

///
/// \brief MainWindow::on_pushButton_clicked
/// 获取exe所在路径
void MainWindow::on_pushButton_clicked()
{
    QString home_path = ".";
    if( !exe_path.isEmpty()  )
        home_path = exe_path;

    exe_path = QFileDialog::getOpenFileName(this, tr("Open File"),
                                                      exe_path,
                                                      tr("*"));

    ui->lineEdit->setText(exe_path);
    //获取exe路径
    exe_dir = "";
    QString path_temp = exe_path;
    QStringList temp_list = path_temp.split("/");
    for( auto path_temp = temp_list.begin() ; path_temp < temp_list.end() - 1 ; path_temp++ )
    {
        if( !path_temp->isEmpty() )
        {
            qDebug() << *path_temp;
            exe_dir += R"(/)" + *path_temp;
        }
    }
    exe_dir += "/";
    QString log = "执行文件所在文件夹:" + exe_dir;
    ui->textEdit->append(log);
    //写入配置文件
    QSettings config("./local.ini",QSettings::IniFormat);
    config.beginGroup("PARA");
    config.setValue("EXE_PATH",exe_path);
    config.setValue("EXE_DIR",exe_dir);
    config.endGroup();
    config.sync();
}

///
/// \brief MainWindow::qprocess_exe_cmd
/// \param cmd
/// \param arg
/// \return
/// linux系统下软件打包工具
QString MainWindow::qprocess_exe_cmd( CMD_TYPE type , QString cmd , QStringList arg )
{
    QProcess process;
    QString cmd_temp;
    //普通权限执行命令
    if( type == user )
    {
        cmd_temp = cmd;
    }
    //超级管理员权限
    if( type == super )
    {
        cmd_temp = "sh -c" ;
        cmd_temp += " \"echo ";
        cmd_temp += passwd;   //密码
        cmd_temp += " | sudo -S ";
        cmd_temp += cmd;
        cmd_temp += "\"";
    }
    qDebug() << cmd_temp;
    process.start(cmd_temp);
    process.waitForFinished();
    QString output = process.readAllStandardOutput();

    QProcess::ProcessError err = process.error();


    //qDebug() << output;
    return output;
}

///
/// \brief MainWindow::copy_files
/// \param files
/// 批量复制文件
void MainWindow::copy_files( QStringList files )
{
    for(auto file : files )
    {
        QString temp_file = file;
        QStringList List_temp = temp_file.split("/");
        QString cmd = "cp " + file + " " + exe_dir + List_temp.at(List_temp.size()-1);
        //qDebug() << cmd;
        qprocess_exe_cmd(super,cmd,QStringList());
    }
}

///
/// \brief MainWindow::on_pushButton_2_clicked
/// 执行打包命令
void MainWindow::on_pushButton_2_clicked()
{
    QString cmd = "ldd " + exe_path;
    qDebug() << cmd;
    QString cmd_ldd = qprocess_exe_cmd( user ,cmd , QStringList());
    ui->textEdit->append(cmd_ldd);
    ui->textEdit->append("解析后需要的库:");
    //解析
    QStringList lib_name = cmd_ldd.split("\n\t");
    for( auto str : lib_name )
    {
        qDebug() << str;
    }
    //解析lib
    QStringList lib_path_list;
    for( auto str : lib_name )
    {
        QStringList list_temp = str.split(" ");
        if( list_temp.size() > 2 )
        {
            lib_path_list.push_back(list_temp.at(2));
            ui->textEdit->append(list_temp.at(2));
        }
    }
    //赋值文件到当地
    copy_files(lib_path_list);
    lib_list_path = lib_path_list;
}

///
/// \brief MainWindow::find_needed_lib_has
/// \param lib_path：库全路径
/// \param tar_lib：目标库
/// \return
///
bool MainWindow::find_needed_lib_has( QString lib_path , QString tar_lib )
{
    QString cmd = "ldd " + lib_path;
    QString cmd_ldd = qprocess_exe_cmd( user ,cmd , QStringList());
    //
    return cmd_ldd.contains(tar_lib, Qt::CaseInsensitive);
}
///
/// \brief MainWindow::on_pushButton_3_clicked
/// 搜索那个库使用了目标库
void MainWindow::on_pushButton_3_clicked()
{
    QString tar_lib = ui->lineEdit_2->text();
    QRegExp reg(".so*");
    //查看输入的是否是.so文件
    //if( reg.exactMatch(tar_lib) )
    {
        //查找
        for( auto lib : lib_list_path )
        {
            if( find_needed_lib_has(lib,tar_lib) )
            {
                QString log = R"(<font color = "#0000FF">)";
                log += lib;
                log += "需要";
                log += tar_lib;
                log += R"(<\font>)";
                ui->textEdit->append(log);
            }
        }
    }
    //else
    {
      //  ui->textEdit->append(R"(<font color= "#FF0000" >输入的不是.so名，请检查</font>)");
    }
}

///
/// \brief MainWindow::on_pushButton_4_clicked
/// 获取超级用户密码
void MainWindow::on_pushButton_4_clicked()
{
    passwd = ui->lineEdit_3->text();

    //写入配置文件
    QSettings config("./local.ini",QSettings::IniFormat);
    config.beginGroup("PARA");
    config.setValue("PASSWD",passwd);
    config.endGroup();
    config.sync();
}
