#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>


typedef enum _CMD_TYPE_
{
    user,
    super
}CMD_TYPE;

namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = nullptr);
    ~MainWindow();

private slots:
    void on_pushButton_clicked();
    //调用QProcess执行命令
    QString qprocess_exe_cmd( CMD_TYPE type ,QString cmd , QStringList arg );
    //
    void on_pushButton_2_clicked();
    //批量拷贝文件
    void copy_files( QStringList files );
    void on_pushButton_3_clicked();
    //查找库是否需要另一个库
    bool find_needed_lib_has( QString lib_path , QString tar_lib );
    //
    void on_pushButton_4_clicked();

private:
    Ui::MainWindow *ui;
    QString exe_path;           //获取exe所在路径
    QString exe_dir;            //exe路径
    QStringList lib_list_path;  //库路径
    QString passwd;             //
};

#endif // MAINWINDOW_H
